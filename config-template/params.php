<?php

return [
	'adminEmail' => 'admin@dudev.ru',
	'api' => require(dirname(dirname(__DIR__)) . '/general/config/api.php'),
	'durationAuth' => 3600 * 24 * 30, //хранить auth_key месяц
];
