<?php

use yii\db\Schema;
use yii\db\Migration;

class m150115_175831_create_temporary_keys_table extends Migration
{
    public function up()
    {
	    $this->createTable('temporary_keys', [
		    'key' => 'CHAR(64) PRIMARY KEY',
		    'service' => 'CHAR(20) NOT NULL',
		    'user_id' => Schema::TYPE_INTEGER . ' NOT NULL',
		    'created_at' => Schema::TYPE_INTEGER . ' UNSIGNED NOT NULL',
	    ]);
    }

    public function down()
    {
        echo "m150115_175831_create_temporary_keys_table cannot be reverted.\n";

        return false;
    }
}
