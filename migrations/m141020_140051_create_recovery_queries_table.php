<?php

use yii\db\Schema;
use yii\db\Migration;

class m141020_140051_create_recovery_queries_table extends Migration
{
    public function up()
    {
	    $this->createTable('recovery_queries', [
		    'id' => Schema::TYPE_BIGPK,
		    'user_id' => Schema::TYPE_INTEGER . ' NOT NULL',
		    'hash' => Schema::TYPE_STRING . ' NOT NULL',
            'service' => Schema::TYPE_STRING . ' NOT NULL',
            'created_at' => Schema::TYPE_INTEGER . ' NOT NULL',
	    ]);
	    $this->createIndex('hash_idx', 'recovery_queries', 'hash', true);
	    $this->createIndex('user_id_idx', 'recovery_queries', 'user_id', true);
    }

    public function down()
    {
        echo "m141020_140051_create_recovery_queries_table cannot be reverted.\n";

        return false;
    }
}
