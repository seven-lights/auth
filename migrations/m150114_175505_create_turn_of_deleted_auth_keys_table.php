<?php

use yii\db\Schema;
use yii\db\Migration;

class m150114_175505_create_turn_of_deleted_auth_keys_table extends Migration
{
    public function up()
    {
	    $this->createTable('turn_auth_key', [
		    'access_key' => 'CHAR(64) PRIMARY KEY',
		    'service' => 'CHAR(20) NOT NULL',
	    ]);
    }

    public function down()
    {
        echo "m150114_175505_create_turn_of_deleted_auth_keys_table cannot be reverted.\n";

        return false;
    }
}
