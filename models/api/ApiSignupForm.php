<?php
namespace app\models\api;

use app\models\SecretQuestion;
use app\models\User;
use Yii;
use yii\base\Model;

/**
 * Api Signup form
 */
class ApiSignupForm extends Model {
	public $login;
	public $password;

	public $question;
	public $answer;

	public function rules() {
		return [
			['login', 'filter', 'filter' => 'trim'],
			['login', 'required'],
			['login', 'unique', 'targetClass' => '\app\models\User', 'message' => 'Введённый вами логин уже занят.'],
			['login', 'string', 'min' => 2, 'max' => 50],

			['password', 'required'],
			['password', 'string', 'min' => 4, 'max' => 50],

			['question', 'string', 'min' => 5, 'max' => 255],
			['answer', 'string', 'min' => 2, 'max' => 255],
		];
	}

	/**
	 * Signs user up.
	 *
	 * @return User|null the saved model or null if saving fails
	 */
	public function signup() {

		if ($this->validate()) {
			$transaction = Yii::$app->db->beginTransaction();

			try {
				$user = new User();
				$user->login = $this->login;
				$user->setPassword($this->password);
				$user->generateAuthKey();
				$user->save();

				if(!empty($this->question)) {
					$secret_question = new SecretQuestion();
					$secret_question->user_id = $user->getId();
					$secret_question->question = $this->question;
					$secret_question->answer = $this->answer;
					$secret_question->save();
				}

				$transaction->commit();
				return $user;
			} catch(\Exception $e) {
				$transaction->rollBack();
				return null;
			}
		}
		return null;
	}
}
