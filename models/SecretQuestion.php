<?php

namespace app\models;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "{{%secret_question}}".
 *
 * @property string $id
 * @property string $question
 * @property string $answer
 * @property string $user_id
 * @property integer $created_at
 * @property integer $updated_at
 */
class SecretQuestion extends ActiveRecord {
	public function behaviors() {
		return [
			TimestampBehavior::className(),
		];
	}

    public function rules() {
        return [
            [['user_id'], 'integer'],
	        ['question', 'string', 'min' => 5, 'max' => 255],
	        ['answer', 'string', 'min' => 2, 'max' => 255],
        ];
    }
}
