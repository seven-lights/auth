<?php

namespace app\models;

use yii\db\ActiveRecord;

/**
 * @property mixed key
 * @property int user_id
 * @property string service
 * @property int created_at
 * @property mixed browser
 * @property mixed ip
 */
class TemporaryKeys extends ActiveRecord {
    public function rules() {
        return [
            [['key', 'service', 'user_id'], 'required'],
            [['user_id', 'created_at'], 'integer'],
            ['key', 'string', 'max' => 64],
	        ['service', 'string', 'max' => 20],
	        ['ip', 'string', 'max' => 39],
	        ['browser', 'string', 'max' => 255],
        ];
    }

	public static function newKey($user_id, $service) {
		$model = new TemporaryKeys();
		$model->key = \Yii::$app->security->generateRandomString(64);
		$model->user_id = $user_id;
		$model->browser = \Yii::$app->request->getUserAgent() ?: '';
		$model->ip = \Yii::$app->request->getUserIP();
		$model->service = $service;
		if($model->save()) {
			return $model;
		}
		return null;
	}

	public function beforeSave($insert) {
		if (parent::beforeSave($insert)) {
			$this->created_at = time();
			return true;
		} else {
			return false;
		}
	}
}
