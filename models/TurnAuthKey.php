<?php

namespace app\models;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;
use yii\db\Query;

/**
 * This is the model class for table "{{%auth_key}}".
 *
 * @property string $auth_key
 * @property integer $service
 */
class TurnAuthKey extends ActiveRecord
{

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['auth_key', 'service'], 'required'],
            [['service'], 'integer'],
            [['auth_key'], 'string', 'max' => 64],
        ];
    }
}
