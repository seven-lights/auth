<?php

namespace app\models;

use Yii;
use yii\base\Exception;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;
use yii\db\Command;
use yii\db\Query;

/**
 * This is the model class for table "{{%auth_key}}".
 *
 * @property string $auth_key
 * @property string $user_id
 * @property string $browser
 * @property string $ip
 * @property string $service
 * @property integer $created_at
 * @property integer $updated_at
 */
class AuthKey extends ActiveRecord
{
    public function behaviors() {
		return [
			TimestampBehavior::className(),
		];
	}

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['auth_key', 'user_id', 'ip', 'service'], 'required'],
            [['user_id'], 'integer'],
            [['auth_key'], 'string', 'max' => 64],
            [['browser'], 'string', 'max' => 255],
            [['ip'], 'string', 'max' => 39]
        ];
    }

	public static function newAuthKey($user_id, $service, $ip, $browser) {
		$model = new AuthKey();
		$model->auth_key = Yii::$app->security->generateRandomString(64);
		$model->user_id = $user_id;
		$model->browser = $browser;
		$model->ip = $ip;
		$model->service = $service;
		if($model->save()) {
			return $model;
		}
		return null;
	}

	public static function revoke($condition) {
		$transaction = Yii::$app->db->beginTransaction();
		try {
			$keys = (new Query())
				->select('auth_key, service')
				->from('auth_key')
				->where($condition)
				->all();

			foreach ($keys as $key) {
				$turn = new TurnAuthKey();
				$turn->auth_key = $key['auth_key'];
				$turn->service = $key['service'];
				$turn->save();
			}

			(new Command(['db' => Yii::$app->db]))->delete(AuthKey::tableName(), $condition)->execute();

			$transaction->commit();
			return true;
		} catch(Exception $e) {
			$transaction->rollBack();
		}
		return false;
	}
}
