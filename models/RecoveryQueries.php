<?php

namespace app\models;

use Yii;
use \yii\db\ActiveRecord;

class RecoveryQueries extends ActiveRecord {
	public function getUser() {
		$this->hasOne(User::className(), ['id' => 'user_id']);
	}
	public static function newQuery($user_id, $service) {
		if(!($model = RecoveryQueries::findOne(['user_id' => $user_id]))) {
			$model = new RecoveryQueries();
			$model->hash = Yii::$app->security->generateRandomString(64);
			$model->service = $service;
			$model->user_id = $user_id;
			if($model->save()) {
				return $model;
			}
		} else {
			return $model;
		}

		return false;
	}
	public function beforeSave($insert) {
		if (parent::beforeSave($insert)) {
			$this->created_at = time();
			return true;
		} else {
			return false;
		}
	}
}
