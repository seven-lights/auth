<?php

namespace app\models;

use Yii;
use yii\base\Exception;
use yii\base\NotSupportedException;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;
use yii\web\IdentityInterface;

class User extends ActiveRecord implements IdentityInterface {
    const STATUS_DELETED = false;
    const STATUS_ACTIVE = true;

	public $password_reset_token;
	public $password;

	static private $_auth_keys = [];

    public function behaviors() {
        return [
            TimestampBehavior::className(),
        ];
    }

    public function rules() {
        return [
	        ['login', 'filter', 'filter' => 'trim'],
	        ['login', 'required'],
	        ['login', 'unique', 'targetClass' => '\app\models\User', 'message' => 'Введённый вами логин уже занят.'],
	        ['login', 'string', 'min' => 2, 'max' => 50],

	        ['password', 'string', 'min' => 4, 'max' => 50],

	        ['status', 'default', 'value' => self::STATUS_ACTIVE],
	        ['status', 'boolean'],
        ];
    }

    public static function findIdentity($id) {
	    return static::findOne(['id' => $id, 'status' => self::STATUS_ACTIVE]);
    }

	public static function findIdentityByAccessToken($token, $type = null) {
		throw new NotSupportedException('"findIdentityByAccessToken" is not implemented.');
	}

    public static function logout() {
        AuthKey::revoke([
	        'user_id' => Yii::$app->user->getId(),
	        'ip' => Yii::$app->request->getUserIP(),
	        'browser' => Yii::$app->request->getUserAgent()
        ]);
	    /* @TODO возможно стоит вернуть false, если не удалось отозвать ключи */
        Yii::$app->user->logout();
    }

	public static function logoutEverywhere() {
		$model = User::findOne(['id' => Yii::$app->user->getId()]);
		$model->generateAuthKey();

		$transaction = \Yii::$app->db->beginTransaction();
		try {
			$model->save();
			AuthKey::revoke([
				'user_id' => Yii::$app->user->getId(),
			]);
			$transaction->commit();
		} catch(Exception $e) {
			$transaction->rollBack();
			/* @TODO возможно стоит вернуть false */
		}
		Yii::$app->user->logout();
	}

    /**
     * Finds user by login
     *
     * @param  string      $login
     * @return static|null
     */
    public static function findByLogin($login) {
	    return static::findOne(['login' => $login, 'status' => self::STATUS_ACTIVE]);
    }

	public static function findByPasswordResetToken($token) {
		$expire = Yii::$app->params['user.passwordResetTokenExpire'];
		$parts = explode('_', $token);
		$timestamp = (int) end($parts);
		if ($timestamp + $expire < time()) {
			// token expired
			return null;
		}

		return RecoveryQueries::find(['reset_hash' => $token])
			->limit(1)
			->with('user')
			->all()[0]->user;
	}

    public function getId() {
        return $this->id;
    }

    public function getAuthKey() {
	    return $this->auth_key;
    }

    public function validateAuthKey($authKey) {
	    return $this->getAuthKey() === $authKey;
    }

    /**
     * Validates password
     *
     * @param  string  $password password to validate
     * @return boolean if password provided is valid for current user
     */
	public function validatePassword($password) {
		return Yii::$app->security->validatePassword($password, $this->password_hash);
	}

	/**
	 * Generates password hash from password and sets it to the model
	 *
	 * @param string $password
	 */
	public function setPassword($password) {
		$this->password_hash = Yii::$app->security->generatePasswordHash($password);
	}

	/**
	 * Generates "remember me" authentication key
	 */
	public function generateAuthKey() {
		$this->auth_key = Yii::$app->security->generateRandomString(64);
	}

	/**
	 * Generates new password reset token
	 */
	public function createPasswordResetToken() {
		$this->password_reset_token = Yii::$app->security->generateRandomString(64);

		$recovery_queries = new RecoveryQueries();
		$recovery_queries->user_id = $this->getId();
		$recovery_queries->hash = $this->password_reset_token;
		$recovery_queries->save();
	}

	/**
	 * Removes password reset token
	 */
	public function removePasswordResetToken() {
		$recovery_queries = RecoveryQueries::findOne($this->getId());
		$recovery_queries->delete();
	}

	public function getSecretQuestions() {
		return $this->hasMany(SecretQuestion::tableName(), ['user_id' => 'id']);
	}
}
