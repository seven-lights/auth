<?php

namespace app\controllers\api;

use app\models\api\ApiSignupForm;
use app\models\AuthKey;
use app\models\LoginForm;
use app\models\SignupForm;
use app\models\TemporaryKeys;
use app\models\User;
use general\controllers\api\Controller;
use yii\base\Exception;
use yii\db;
use yii\data\Pagination;
use yii\web\Response;

class UserController extends Controller {
	protected $_safe_actions = [
		'get-mark',
		'is-authenticate',
		'authenticate',
		'signup-form',
		'logout',
		'logoutEverywhere',
	];
	public $layout = 'empty';
    public function actionSearch($page = 0) {
	    if($page > 100) {
		    return $this->sendError(self::ERROR_TOO_BIG_PAGE_NUMBER);
	    }

	    $search = \Yii::$app->request->post('search', []);
	    //Удаляем левые параметры
	    foreach($search as $key => $value) {
		    if(!in_array($key, ['id', 'login', 'status'], true)) {
			    unset($search[ $key ]);
		    }
	    }

	    //Считаем кол-во пользователей
	    $totalCount = (new db\Query())->from('user')->where($search)->count();
	    if($totalCount == 0) {
		    return $this->sendError(self::ERROR_NO_USER);
	    }
	    $pages = new Pagination(['totalCount' => $totalCount]);
	    $pages->setPage($page);
	    $pages->setPageSize(30);

	    $res = (new db\Query())
		    ->select('id, login, status')
		    ->from('user')
		    ->where($search)
		    ->offset($pages->offset)
		    ->limit($pages->limit)
		    ->all();

	    if($res === []) {
		    return $this->sendError(self::ERROR_DB);
	    }

	    return $this->sendSuccess([
		    'users' => $res,
		    'countPages' => $pages->getPageCount(),
		    'page' => $pages->getPage()
	    ]);
    }
	public function actionSignup() {
		$model = new ApiSignupForm();
		$model->setAttributes(\Yii::$app->request->post('user'));
		if ($user = $model->signup()) {
			return $this->sendSuccess([
				'user' => [
					'id' => $user->getId(),
					'login' => $user->login,
					'status' => $user->status,
				]
			]);
		} else {
			if($model->hasErrors()) {
				$error_codes = [
					'login' => self::ERROR_ILLEGAL_LOGIN,
					'password' => self::ERROR_ILLEGAL_PASSWORD,
					'question' => self::ERROR_ILLEGAL_QUESTION,
					'answer' => self::ERROR_ILLEGAL_ANSWER,
				];
				foreach ($model->getFirstErrors() as $attr => $err) {
					if(isset($error_codes[ $attr ])) {
						$errors[] = $error_codes[ $attr ];
					}
				}
			} else {
				$errors = self::ERROR_DB;
			}
		}
		if(!isset($errors)) {
			$errors = self::ERROR_UNKNOWN;
		}
		return $this->sendError($errors);
	}
	public function actionDelete($user_id) {
		if($user = User::findOne(['id' => $user_id])) {
			if($user->delete() === false) {
				return $this->sendError(self::ERROR_DB);
			} else {
				return $this->sendSuccess([]);
			}
		} else {
			return $this->sendError(self::ERROR_NO_USER);
		}
	}
	public function actionUpdate($user_id) {
		/** @var User $model */
		if($model = User::findOne(['id' => $user_id])) {
			$model->setAttributes(\Yii::$app->request->post('user'));
			if(!$model->validate()) {
				$error_codes = [
					'login' => self::ERROR_ILLEGAL_LOGIN,
					'password' => self::ERROR_ILLEGAL_PASSWORD,
					'status' => self::ERROR_ILLEGAL_STATUS,
				];
				foreach ($model->getFirstErrors() as $attr => $err) {
					if(isset($error_codes[ $attr ])) {
						$errors[] = $error_codes[ $attr ];
					}
				}
			} else {
				if(isset($model->password)) {
					$model->setPassword($model->password);
				}
				if($model->save()) {
					return $this->sendSuccess([
						'user' => [
							'id' => $model->getId(),
							'login' => $model->login,
							'status' => $model->status,
						]
					]);
				} else {
					$errors = self::ERROR_DB;
				}
			}
		} else {
			$errors = self::ERROR_NO_USER;
		}
		if(!isset($errors)) {
			$errors = self::ERROR_UNKNOWN;
		}
		return $this->sendError($errors);
	}
	public function actionChangePassword($user_id, $oldPassword, $password) {
		/** @var User $model */
		if($model = User::findOne(['id' => $user_id])) {
			if($model->validatePassword($oldPassword)) {
				$model->password = $password;
				if(!$model->validate()) {
					$error_codes = [
						'password' => self::ERROR_ILLEGAL_PASSWORD,
					];
					foreach ($model->getFirstErrors() as $attr => $err) {
						if(isset($error_codes[ $attr ])) {
							$errors[] = $error_codes[ $attr ];
						}
					}
				} else {
                    $model->setPassword($password);
                    if ($model->save()) {
                        return $this->sendSuccess([]);
                    } else {
                        $errors = self::ERROR_DB;
                    }
                }
			} else {
				$errors = self::ERROR_INCORRECT_LOGIN_PASSWORD;
			}
		} else {
			$errors = self::ERROR_NO_USER;
		}
		if(!isset($errors)) {
			$errors = self::ERROR_UNKNOWN;
		}
		return $this->sendError($errors);
	}
	public function actionUpdateAuthKey($user_id) {
		/** @var User $model */
		if($model = User::findOne(['id' => $user_id])) {
			$model->generateAuthKey();

			$transaction = \Yii::$app->db->beginTransaction();
			try {
				$model->save();
				AuthKey::revoke([
					'user_id' => \Yii::$app->user->getId()
				]);
				$transaction->commit();
				return $this->sendSuccess([]);
			} catch(Exception $e) {
				$transaction->rollBack();
				$errors = self::ERROR_DB;
			}
		} else {
			$errors = self::ERROR_NO_USER;
		}
		if(!isset($errors)) {
			$errors = self::ERROR_UNKNOWN;
		}
		return $this->sendError($errors);
	}
	public function actionGetServiceAuthKey($service, $key) {
		if($mark = TemporaryKeys::findOne(['key' => $key, 'service' => $service])) {
			/* @var $mark TemporaryKeys */
			$transaction = \Yii::$app->db->beginTransaction();
			try {
				$model = AuthKey::newAuthKey($mark->user_id, $service, $mark->ip, $mark->browser);
				$mark->delete();
				$transaction->commit();
				return $this->sendSuccess([
					'auth_key' => $model->auth_key,
					'user_id' => $model->user_id,
				]);
			} catch(Exception $e) {
				$transaction->rollBack();
				$errors = self::ERROR_DB;
			}
		} else {
			$errors = self::ERROR_INCORRECT_MARK;
		}
		if(!isset($errors)) {
			$errors = self::ERROR_UNKNOWN;
		}
		return $this->sendError($errors);
	}
	public function actionGetMark($service, $callback, $sign, $rand) {
		//неверный ник сервиса
		if(!isset(\Yii::$app->params['api']['secrets'][ $service ])) {
			return $this->sendError(self::ERROR_FORBIDDEN);
		}
		//проверяем подпись
		$computedSing = md5(
			$service .
			md5(
				'service=' . $service .
				'&callback=' . $callback .
				'&rand=' . $rand
			) .
			'get-mark' .
			\Yii::$app->params['api']['secrets'][ $service ]
		);
		if($computedSing !== $sign) {
			return $this->sendError(self::ERROR_INCORRECT_SIGN);
		}
		//недопустимое имя функции обратного вызова
		if(!preg_match('#^[a-zA-Z]+$#', $callback)) {
			return $this->sendError(self::ERROR_ILLEGAL_CALLBACK);
		}

		\Yii::$app->response->format = Response::FORMAT_JSONP;
		if(\Yii::$app->user->isGuest) {
			return [
				'data' => $this->sendError(self::ERROR_NOT_AUTHENTICATED),
				'callback' => $callback,
			];
		}

		if($model = TemporaryKeys::newKey(\Yii::$app->user->id, $service)) {
			return [
				'data' => $this->sendSuccess([
					'key' => $model->key
				]),
				'callback' => $callback,
			];
		} else {
			return [
				'data' => $this->sendError(self::ERROR_DB),
				'callback' => $callback,
			];
		}
	}
	public function actionIsAuthenticate($service, $callback, $sign, $rand) {
		//неверный ник сервиса
		if(!isset(\Yii::$app->params['api']['secrets'][ $service ])) {
			return $this->sendError(self::ERROR_FORBIDDEN);
		}
		//проверяем подпись
		$computedSing = md5(
			$service .
			md5(
				'service=' . $service .
				'&callback=' . $callback .
				'&rand=' . $rand
			) .
			'is-authenticate' .
			\Yii::$app->params['api']['secrets'][ $service ]
		);
		if($computedSing !== $sign) {
			return $this->sendError(self::ERROR_INCORRECT_SIGN);
		}
		//недопустимое имя функции обратного вызова
		if(!preg_match('#^[a-zA-Z]+$#', $callback)) {
			return $this->sendError(self::ERROR_ILLEGAL_CALLBACK);
		}

		\Yii::$app->response->format = Response::FORMAT_JSONP;
		return [
			'data' => $this->sendSuccess([
				'is_authenticate' => !\Yii::$app->user->isGuest,
			]),
			'callback' => $callback,
		];
	}
	public function actionAuthenticate($service, $view, $retUrl, $sign, $rand) {
		$this->layout = 'emptyHtml';
		//неверный ник сервиса
		if(!isset(\Yii::$app->params['api']['secrets'][ $service ])) {
			return $this->redirectApi('error', self::ERROR_FORBIDDEN, $retUrl);
		}
		//проверяем подпись
		$computedSing = md5(
			$service .
			md5(
				'service=' . $service .
				'&view=' . $view .
				'&retUrl=' . $retUrl .
				'&rand=' . $rand
			) .
			'authenticate' .
			\Yii::$app->params['api']['secrets'][ $service ]
		);
		if($computedSing !== $sign) {
			return $this->redirectApi('error', self::ERROR_INCORRECT_SIGN, $retUrl);
		}
		//недопустимое имя шаблона
		if(!preg_match('#^[a-zA-Z0-9]+$#', $view)) {
			return $this->redirectApi('error', self::ERROR_ILLEGAL_SCHEDULE, $retUrl);
		}

		$form = new LoginForm();
		if(\Yii::$app->user->isGuest) {
			if (!$form->load(\Yii::$app->request->post()) || !$form->login()) {
				return $this->render($service . '/' . $view, [
					'model' => $form,
					'service' => $service,
					'view' => $view,
					'retUrl' => $retUrl,
				]);
			}
		}

		if($model = TemporaryKeys::newKey(\Yii::$app->user->id, $service)) {
			return $this->redirectApi('success', [
				'key' => $model->key,
			], $retUrl);
		} else {
			$form->addError('db', 'Ошибка базы данных');
			return $this->render($service . '/' . $view, [
				'model' => $form,
				'service' => $service,
				'view' => $view,
				'retUrl' => $retUrl,
			]);
		}
	}
	public function actionLogout($service, $retUrl, $sign, $rand) {
		//неверный ник сервиса
		if(!isset(\Yii::$app->params['api']['secrets'][ $service ])) {
			return;
		}
		//проверяем подпись
		$computedSing = md5(
			$service .
			md5(
				'service=' . $service .
				'&retUrl=' . $retUrl .
				'&rand=' . $rand
			) .
			'logout' .
			\Yii::$app->params['api']['secrets'][ $service ]
		);
		if($computedSing !== $sign) {
			return;
		}
		User::logout();
		$this->redirect($retUrl);
	}
	public function actionLogoutEverywhere($service, $retUrl, $sign, $rand) {
		//неверный ник сервиса
		if(!isset(\Yii::$app->params['api']['secrets'][ $service ])) {
			return;
		}
		//проверяем подпись
		$computedSing = md5(
			$service .
			md5(
				'service=' . $service .
				'&retUrl=' . $retUrl .
				'&rand=' . $rand
			) .
			'logout' .
			\Yii::$app->params['api']['secrets'][ $service ]
		);
		if($computedSing !== $sign) {
			return;
		}
		User::logoutEverywhere();
		$this->redirect($retUrl);
	}
	public function actionSignupForm($service, $view, $retUrl, $sign, $rand) {
		$this->layout = 'emptyHtml';
		//неверный ник сервиса
		if(!isset(\Yii::$app->params['api']['secrets'][ $service ])) {
			return $this->redirectApi('error', self::ERROR_FORBIDDEN, $retUrl);
		}
		//проверяем подпись
		$computedSing = md5(
			$service .
			md5(
				'service=' . $service .
				'&view=' . $view .
				'&retUrl=' . $retUrl .
				'&rand=' . $rand
			) .
			'signup-form' .
			\Yii::$app->params['api']['secrets'][ $service ]
		);
		if($computedSing !== $sign) {
			return $this->redirectApi('error', self::ERROR_INCORRECT_SIGN, $retUrl);
		}
		//недопустимое имя шаблона
		if(!preg_match('#^[a-zA-Z0-9]+$#', $view)) {
			return $this->redirectApi('error', self::ERROR_ILLEGAL_SCHEDULE, $retUrl);
		}

		if(\Yii::$app->user->isGuest) {
			$form = new SignupForm();
			if (!$form->load(\Yii::$app->request->post()) || !$form->signup()) {
				return $this->render($service . '/' . $view, [
					'model' => $form,
					'service' => $service,
					'view' => $view,
					'retUrl' => $retUrl,
				]);
			}
		}

		return $this->redirectApi('success', [], $retUrl);
	}
	/**
	 * Редирект для апи
	 * @param $retUrl string
	 * @param $result string
	 * @param $data string|array
	 * @return Response the current response object
	 */
	private function redirectApi($result, $data, $retUrl) {
		$url = $retUrl . '?result=' . $result;
		switch($result) {
			case 'error':
				$url .= '&errors[0][code]=' . $data . '&errors[0][text]=' . Controller::$_errors[ $data ];
				break;
			case 'success':
				foreach($data as $key => $value) {
					$url .= '&' . $key . '=' . urlencode($value);
				}
				break;
		}
		return $this->redirect($url);
	}
}
