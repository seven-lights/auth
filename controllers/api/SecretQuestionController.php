<?php

namespace app\controllers\api;

use app\models\SecretQuestion;
use app\models\User;
use general\controllers\api\Controller;
use yii\db;

class SecretQuestionController extends Controller {
	public $layout = 'empty';
	public function actionGet($user_id) {
		if($model = SecretQuestion::findOne(['user_id' => $user_id])) {
			return $this->sendSuccess([
				'question' => $model->question,
			]);
		} else {
			$errors = self::ERROR_NO_SECRET_QUESTION;
		}
		if(!isset($errors)) {
			$errors = self::ERROR_UNKNOWN;
		}
		return $this->sendError($errors);
	}
	public function actionUpdate($user_id, array $question) {
		if(!($model = SecretQuestion::findOne(['user_id' => $user_id]))) {
			$model = new SecretQuestion();
		} else {
			if($question['check_answer']
				&& $model->answer !== $question['old_answer']) {
				$errors = self::ERROR_INCORRECT_ANSWER;
			}
		}
		if(!isset($errors)) {
			$model->setAttributes($question);
			$model->user_id = $user_id;

			if(!$model->validate()) {
				$error_codes = [
					'question' => self::ERROR_ILLEGAL_QUESTION,
					'answer' => self::ERROR_ILLEGAL_ANSWER,
				];
				foreach ($model->getFirstErrors() as $attr => $err) {
					if(isset($error_codes[ $attr ])) {
						$errors[] = $error_codes[ $attr ];
					}
				}
			} else {
				if($model->save()) {
					return $this->sendSuccess([
						'question' => $model->question,
					]);
				} else {
					$errors = self::ERROR_DB;
				}
			}
		}
		if(!isset($errors)) {
			$errors = self::ERROR_UNKNOWN;
		}
		return $this->sendError($errors);
	}
	public function actionRestorePassword($user_id, $answer, $password) {
		if($user = User::findOne(['id' => $user_id])) {
			if($sq = SecretQuestion::findOne(['user_id' => $user_id])) {
				if($sq->answer === $answer) {
					$user->setAttribute('password', $password);
					if($user->hasErrors('password')) {
						$errors = self::ERROR_ILLEGAL_PASSWORD;
					} else {
						$user->setPassword($user->password);
						if($user->save()) {
							return $this->sendSuccess([]);
						} else {
							$errors = self::ERROR_DB;
						}
					}
				} else {
					$errors = self::ERROR_INCORRECT_ANSWER;
				}
			} else {
				$errors = self::ERROR_NO_SECRET_QUESTION;
			}
		} else {
			$errors = self::ERROR_NO_USER;
		}
		return $this->sendError(isset($errors) ?: self::ERROR_UNKNOWN);
	}
}
